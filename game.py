from sys import argv
from ccCyberPunk.game import Game

def main():
	if len(argv) == 1: #no arguments

		with open('game_description.txt') as f:
			print f.read()
	
	elif len(argv) >= 2:
		if argv[1] == 'list':
			#list all the saved games
			pass
		elif argv[1] == 'resume':
			#resume a saved game
			pass
		elif argv[1] == 'new':
			#start a new game
			if len(argv) >= 3:
				Game().start(argv[2])
			else:
				Game('new game').start()


if __name__ == '__main__':
	main()